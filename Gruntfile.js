module.exports = function(grunt) {
  'use strict';

  grunt.initConfig({
    dev_url: 'baeldung.loc', // URL of your local install
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      gruntfile: [
        "Gruntfile.js"
      ],
      all: [
        'library/js/*.js'
      ]
    },
    uglify: {
      dist: {
        files: {
          'library/dist/js/scripts.min.js': [
            'bower_components/bootstrap/js/transition.js',
            'bower_components/bootstrap/js/collapse.js',
            'library/js/svg-injector.js',
            'library/js/scripts.js'
          ],
          'library/dist/js/megamenu.min.js': [
            'library/js/megamenu.js'
          ],
          'library/dist/js/vat.min.js': [
            'library/js/vat-rates.js',
            'library/js/vat-calc.js'
          ],
          'library/dist/js/featherlight.js': [
            'library/js/featherlight.js',
          ],
        },
        options: {
          // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
          // sourceMap: 'assets/js/scripts.min.js.map',
          // sourceMappingURL: '/app/themes/roots/assets/js/scripts.min.js.map'
        }
      }
    },
	copy: {
	  main: {
		files: [
		  {expand: true, flatten: true, src: ['library/js/featherlight.css'], dest: 'library/dist/css/'},
		],
	  },
	},
    grunticon: {
      myIcons: {
        files: [{
          expand: true,
          cwd: 'library/img',
          src: ['*.svg', '*.png'],
          dest: "library/img"
        }],
        options: {
        }
      }
    },
    version: {
      assets: {
        files: {
          'functions.php': ['library/dist/js/scripts.min.js']
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      gruntfile: {
        files: 'Gruntfile.js',
        tasks: ['jshint:gruntfile'],
      },
      js: {
        files: [
          '<%= jshint.all %>'
        ],
        tasks: ['uglify']
      },
      all: {
        files: [
          'library/js/**/*.js',
          'library/*.php',
          '*.php',
          'style.css'
        ]
      }
    },
    browserSync: {
      dev: {
        options: {
          watchTask: true,
          proxy: '<%= dev_url %>',
          files: [
            'library/js/**/*.js',
            'library/*.php',
            '*.php',
            'style.css'
          ],
          snippetOptions: {
            whitelist: ['/wp-admin/admin-ajax.php'],
            blacklist: ['/wp-admin/**']
          }
        }
      }
    },
    clean: {
      dist: [
        'library/dist/css',
        'library/dist/js'
      ],
      zip: [
        'baeldung.zip'
      ]
    },
    compress: {
      main: {
        options: {
          archive: 'baeldung.zip'
        },
        files: [
          {
            src: [
              'bower_components/**/*',
              'favicon/**/*',
              'icon/**/*',
              'images/**/*',
              'languages/**/*',
              'library/dist/**/*',
              'library/*.php',
              '*.{php,css,ico,png,md}'
            ],
            dest: '/baeldung/'
          }
        ]
      }
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-wp-assets');
  grunt.loadNpmTasks('grunt-grunticon');
  grunt.loadNpmTasks('grunt-svgstore');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-browser-sync');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'uglify',
	'copy',
    'grunticon',
    'version',
    'zip',
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'uglify',
    'grunticon',
    'version'
  ]);

  grunt.registerTask('dev', [
    'grunticon',
    'browserSync',
    'watch'
  ]);

  grunt.registerTask('zip', [
    'clean:zip',
    'compress'
  ]);

};
