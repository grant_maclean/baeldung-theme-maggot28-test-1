<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

  <div id="sidebar1" class="sidebar flex-col" role="complementary" style="overflow: hidden; max-height:100px;">

    <div class="sticky-widgets" style="visibility: hidden;">
      <?php dynamic_sidebar( 'sidebar1' ); ?>
    </div>

  </div>

<?php endif; ?>
