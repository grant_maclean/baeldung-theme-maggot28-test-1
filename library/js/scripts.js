/* imgsizer (flexible images for fluid sites) */
var imgSizer={Config:{imgCache:[],spacer:"/path/to/your/spacer.gif"},collate:function(aScope){var isOldIE=(document.all&&!window.opera&&!window.XDomainRequest)?1:0;if(isOldIE&&document.getElementsByTagName){var c=imgSizer;var imgCache=c.Config.imgCache;var images=(aScope&&aScope.length)?aScope:document.getElementsByTagName("img");for(var i=0;i<images.length;i++){images[i].origWidth=images[i].offsetWidth;images[i].origHeight=images[i].offsetHeight;imgCache.push(images[i]);c.ieAlpha(images[i]);images[i].style.width="100%";}
if(imgCache.length){c.resize(function(){for(var i=0;i<imgCache.length;i++){var ratio=(imgCache[i].offsetWidth/imgCache[i].origWidth);imgCache[i].style.height=(imgCache[i].origHeight*ratio)+"px";}});}}},ieAlpha:function(img){var c=imgSizer;if(img.oldSrc){img.src=img.oldSrc;}
var src=img.src;img.style.width=img.offsetWidth+"px";img.style.height=img.offsetHeight+"px";img.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+src+"', sizingMethod='scale')"
img.oldSrc=src;img.src=c.Config.spacer;},resize:function(func){var oldonresize=window.onresize;if(typeof window.onresize!='function'){window.onresize=func;}else{window.onresize=function(){if(oldonresize){oldonresize();}
func();}}}}

// add twitter bootstrap classes and color based on how many times tag is used
function addTwitterBSClass(thisObj) {
  var title = jQuery(thisObj).attr('title');
  if (title) {
    var titles = title.split(' ');
    if (titles[0]) {
      var num = parseInt(titles[0]);
      if (num > 0)
        jQuery(thisObj).addClass('label label-default');
      if (num == 2)
        jQuery(thisObj).addClass('label label-info');
      if (num > 2 && num < 4)
        jQuery(thisObj).addClass('label label-success');
      if (num >= 5 && num < 10)
        jQuery(thisObj).addClass('label label-warning');
      if (num >=10)
        jQuery(thisObj).addClass('label label-important');
    }
  }
  else
    jQuery(thisObj).addClass('label');
  return true;
}


// as the page loads, call these scripts
jQuery(document).ready(function($) {

  // modify tag cloud links to match up with twitter bootstrap
  $("#tag-cloud a").each(function() {
      addTwitterBSClass(this);
      return true;
  });
  
  $("p.tags a").each(function() {
    addTwitterBSClass(this);
    return true;
  });
  
  $("ol.commentlist a.comment-reply-link").each(function() {
    $(this).addClass('btn btn-success btn-mini');
    return true;
  });
  
  $('#cancel-comment-reply-link').each(function() {
    $(this).addClass('btn btn-danger btn-mini');
    return true;
  });
  
  // $('article.post').hover(function(){
  //  $('a.edit-post').show();
  // },function(){
  //  $('a.edit-post').hide();
  // });
  
  // Prevent submission of empty form
  $('[placeholder]').parents('form').submit(function() {
    $(this).find('[placeholder]').each(function() {
      var input = $(this);
      if (input.val() === input.attr('placeholder')) {
        input.val('');
      }
    });
  });

  // Open all image links in a new tab.
  $('.post-content img').parent('a').attr('target', '_blank');
});


(function(window, document){
  var svg_event = document.createEvent('Event');
  svg_event.initEvent('ready-to-animate-svg', true, true);
  // Options
  var injectorOptions = {
      evalScripts: 'once',
  };

  // Do the injection
  new SVGInjector(injectorOptions).inject(document.querySelectorAll('svg[data-src]'), function (afterAllInjectionsFinishedCallback){
  document.dispatchEvent(svg_event);
  });
}(window, document));


/*
 * Debounced resize
 */
function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this,
        args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };
    if (immediate && !timeout) {
      func.apply(context, args);
    }
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
}

(function($){ // jQuery ready

  // Adjust the main menu position according to the ribbon height/position
  var watchRibbon = debounce(function() {

    var ribbonHeight = 0; // ribbon initil height

    // find all available ribbons on a page
    $('div.tve-leads-ribbon[data-position="top"]:visible').each(function(){
      // filter only visible ribbons
      if ( $(this).css('opacity') !== '0' ) {
        // collect the height value
        ribbonHeight += $(this).outerHeight();
      }
    });

    // update the body top margin
    $('body').attr('style', 'transition: margin .3s ease-out; margin-top: ' + ribbonHeight + 'px !important');

  }, 100);



  $(window).on('resize', watchRibbon); // watch ribbons on resize

  $(window).on('load', function(){
    // make initiall cal with a triggering event handlers
    $(window).trigger('resize');
    $(window).trigger('scroll');
  });
  
  //add anchor links to h2 & h3 elements on single posts
  $(document).ready(function(){
	//if current page is single post
	if ( $('body').hasClass('single-post') ){
		//for each h2 or h3
		$('h2,h3').each(function(){
			//var holding svg data
			var headingID = $(this).attr('data-id');
			if( (headingID != '') && (headingID !=undefined) ){
				var svg = '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="482.136px" height="482.135px" viewBox="0 0 482.136 482.135" style="enable-background:new 0 0 482.136 482.135;" xml:space="preserve"><g><path d="M455.482,198.184L326.829,326.832c-35.535,35.54-93.108,35.54-128.646,0l-42.881-42.886l42.881-42.876l42.884,42.876   c11.845,11.822,31.064,11.846,42.886,0l128.644-128.643c11.816-11.831,11.816-31.066,0-42.9l-42.881-42.881   c-11.822-11.814-31.064-11.814-42.887,0l-45.928,45.936c-21.292-12.531-45.491-17.905-69.449-16.291l72.501-72.526   c35.535-35.521,93.136-35.521,128.644,0l42.886,42.881C491.018,105.045,491.018,162.663,455.482,198.184z M201.206,366.698   l-45.903,45.9c-11.845,11.846-31.064,11.817-42.881,0l-42.884-42.881c-11.845-11.821-11.845-31.041,0-42.886l128.646-128.648   c11.819-11.814,31.069-11.814,42.884,0l42.886,42.886l42.876-42.886l-42.876-42.881c-35.54-35.521-93.113-35.521-128.65,0   L26.655,283.946c-35.538,35.545-35.538,93.146,0,128.652l42.883,42.882c35.51,35.54,93.11,35.54,128.646,0l72.496-72.499   C246.724,384.578,222.588,379.197,201.206,366.698z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';
				//build anchor html for this heading
				var $anchor = $('<a class="anchor-link" href="#'+headingID+'">'+svg+'</a>');
				//append anchor element to this heaading
				$(this).append($anchor);
			}
		});
	}
  });

  function do_sticky_widget_calcs(max_attempts){
	  
	//this is to stop potential infinte loops if for some reason thrive leads fails to load
	max_attempts--;
	if(max_attempts==0){ return;}
	
	var total_widget_weight = 0;
	var total_widget_height = $('.sticky-widgets').height();;
	var article_height = $('section.post-content').height() + $('.before-post-widgets').height() + $('header').height() - 300;
	var final_widget_height = 0;
	
	//check if thrive leads have all loaded
	// (Thive leads widgets use a temporary container that ends up being removed)
	if( $('.sticky-widgets > .tl-widget-container').length != 0 ){
		//give it a second to load
		setTimeout(function(){do_sticky_widget_calcs(max_attempts);}, 1000);
		return;
	}

	//loop through all the widgets and check for page height condition
	$('.sticky-widgets > div').each(function(){
		page_height_limit = parseInt( $(this).data('height-limit') );
		if(page_height_limit > article_height){
			$(this).addClass('height-limit-hide');
			$(this).css('display', 'none');
		}
	});
	
	//loop through all the widgets (that should not be hidden) to get the total sticky weight
	$('.sticky-widgets > div').each(function(){
		if( !$(this).hasClass('height-limit-hide') ){
			total_widget_weight += parseInt( $(this).data('sticky-weight') );
		}	
	});
	
	
	//if the sidebar is not higher than the article content
	if (total_widget_height < article_height){
		$('.sticky-widgets > div').each(function(){

			fraction_height = parseInt( $(this).data('sticky-weight') ) / total_widget_weight;
			widget_container_height = Math.round(article_height * fraction_height);

			if( widget_container_height > $(this).height() ){
				$(this).css( "height", widget_container_height + "px");
				final_widget_height += widget_container_height;
			}else{
				//console.log('widget c height (not changed): ' + $(this).height() );
			}
			$(this).children().first().css("position", "sticky");
			$(this).children().first().css("top", "105px"); //offset height of nav
		});
	}
	
	//remove the floating property of ezoic ads so that they flow correctly on the page
	//$.css doesn't support !important, hence the use of $.attr	
	$('.textwidget > .ezoic-ad').each(function(){
		$(this).attr('style', $(this).attr('style')+'float: none !important;');
	});
	
	//remove the sidebar styles that allow us to calculate the article height without the sidebar
	$('#sidebar1').attr('style', '');
	//remove the styles that allow us to calculate widget height without them being displayed, and set up for fade in
	$('.sticky-widgets').css('visibility', 'visible');
	$('.sticky-widgets').css('display', 'none');
	//now that the calcs and css is in place, fade the widgets in
	$('.sticky-widgets').css('height', '100%').fadeIn();	  
  }
  $(window).load(function(){
	  
	do_sticky_widget_calcs(10);
	
  });
  
})(jQuery);
