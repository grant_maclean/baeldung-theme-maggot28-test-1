<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  
  <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
    
    <header class="page-header">
      
      <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
    
    </header> <!-- end article header -->
  
    <section class="post-content clearfix" itemprop="text">

      <?php the_content(); ?>
  
    </section> <!-- end article section -->
    
    <!--<footer>

      <?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","wpbootstrap") . ':</span> ', ', ', '</p>'); ?>
      
    </footer>--> <!-- end article footer -->
  
  </article> <!-- end article -->
  
  <?php if ( empty(get_post_meta( $post->ID, 'hide_comments', true )) ) : ?>
    <?php comments_template('', true); ?>
  <?php endif; ?>
  
  <?php endwhile; ?>    
  
  <?php else : ?>
  
  <article id="post-not-found">
      <header class="page-header">
        <h1 class="page-title"><?php _e("Not Found", "wpbootstrap"); ?></h1>
      </header>
      <section class="post-content">
        <p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
      </section>
  </article>
  
  <?php endif; ?>

</div> <!-- end #main -->

<?php get_sidebar(); // sidebar 1 ?>

<?php get_footer(); ?>
