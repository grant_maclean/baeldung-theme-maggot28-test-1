<?php get_header(); ?>

  <div class="page-header">
    <h1 class="page-title">
      <span><?php _e("All articles","wpbootstrap"); ?></span> 
      <?php echo esc_attr(get_search_query()); ?>
    </h1>
  </div>

  <?php if (have_posts()) : ?>

    <div class="archive-columns layout-stacked click-whole no-more rounded-on">

      <?php while (have_posts()) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

        <div class="post-inner">

          <h3 class="post-title">
            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
              <span class="hover-wrapper"><?php the_title(); ?></span>
            </a>
          </h3>

          <section class="post-content">
            <?php the_excerpt(); ?>
          </section> <!-- end post-content -->

        </div> <!-- end post-inner -->

      </article> <!-- end article -->

      <?php endwhile; ?>

    </div>

  <?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>

    <?php wp_bootstrap_page_navi(); // use the page navi function ?>

  <?php } else { // if it is disabled, display regular wp prev & next links ?>
    <nav class="wp-prev-next">
      <ul class="pager">
        <li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
        <li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
      </ul>
    </nav>
  <?php } ?>

  <?php else : ?>

  <article id="post-not-found">
      <header>
        <h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
      </header>
      <section class="post-content">
        <p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
      </section>
      <footer>
      </footer>
  </article>

  <?php endif; ?>

</div> <!-- end #main -->

<?php get_sidebar(); // sidebar 1 ?>

<?php get_footer(); ?>
