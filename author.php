<?php get_header(); ?>

  <div class="page-header">
    <?php
      // If google profile field is filled out on author profile, link the author's page to their google+ profile page
      $curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
      $google_profile = get_the_author_meta( 'google_profile', $curauth->ID );
      if ( $google_profile ) {
        $author_name = '<a href="' . esc_url( $google_profile ) . '" rel="me">' . $curauth->display_name . '</a>';
      } else {
        $author_name = get_the_author_meta('display_name', $curauth->ID);
      }
    ?>

    <div class="flex-col profile-thumb-wrapper">
      <?php if (validate_gravatar(get_query_var( 'author' ))) : ?>
        <p class="profile-picture"><?php echo get_avatar( get_query_var( 'author' ), '456' ); ?></p>
      <?php else: ?>
        <p class="profile-picture profile-default"><img src="<?php echo get_template_directory_uri(); ?>/images/profile-default-image.png" alt="<?php echo $author_name; ?>"></p>
      <?php endif; ?>
    </div>

    <div class="flex-col profile-contact-links">
      <?php

        $c_fields_custom = array('url' => __('Website'));
        $c_fields = array_merge($c_fields_custom, wp_get_user_contact_methods(get_query_var( 'author' )));

        $html = '';

        foreach ($c_fields as $f_key => $f_name) {
          $meta_field = get_the_author_meta($f_key);

          if (!empty($meta_field)) {
            $html .= sprintf('<li><a href="%s">%s &rarr;</a></li>', $meta_field, $f_name);
          }
        }
      ?>

      <?php if(!empty($html)): ?>
        <h3 class="author-contacts-title"><?php _e('Also find me here:'); ?></h3>
        <ul class="author-contact-list"><?php echo $html; ?></ul>
      <?php endif; ?>
    </div>
	
    <div class="flex-col profile-details">
      <h1 class="author-nicename"><?php _e('Baeldung Author', 'wpbootstrap'); ?></h1>
      <h2 class="author-fullname"><?php echo $author_name; ?></h2>
      <p class="author-description"><?php echo the_author_meta( 'description') ?></p>
    </div>

  </div>

  <h2 class="author-posts-title"><?php _e("Here's what I've written (so far):", "wpbootstrap"); ?></h2>

  <div class="author-posts">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

      <header>

        <h3 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

        <p class="post-meta"><span class="text"><?php _e("Filed under", "wpbootstrap"); ?></span> <span class="category"><?php the_category(', '); ?></span></p>

      </header> <!-- end article header -->

      <section class="post-content">

        <?php //the_post_thumbnail( 'wpbs-featured' ); ?>

        <?php the_excerpt(); ?>

        <p><a href="<?php the_permalink() ?>" class="more-link"><?php _e('Read More', 'wpbootstrap'); ?> &rarr;</a></p>

      </section> <!-- end article section -->

    </article> <!-- end article -->

    <?php endwhile; ?>

    <?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>

      <?php wp_bootstrap_page_navi(); // use the page navi function ?>

    <?php } else { // if it is disabled, display regular wp prev & next links ?>
      <nav class="wp-prev-next">
        <ul class="clearfix">
          <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
          <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
        </ul>
      </nav>
    <?php } ?>


    <?php else : ?>

    <article id="post-not-found">
        <header>
          <h1><?php _e("No Posts Yet", "wpbootstrap"); ?></h1>
        </header>
        <section class="post-content">
          <p><?php _e("Sorry, What you were looking for is not here.", "wpbootstrap"); ?></p>
        </section>
    </article>

    <?php endif; ?>

  </div> <!-- end .author-posts -->

</div> <!-- end #main -->

<?php get_footer(); ?>
